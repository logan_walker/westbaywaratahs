Steps to running the website:


1. Open MAMP > Preferences > Web Server > Select...

2. Select the 'westbaywaratahs' folder as the Document Root

3. Access in file explorer westbaywaratahs > sql > localhost.sql

4. Open localhost.sql as a text document

5. Copy all contents of localhost.sql

6. In a web browser type in the url "localhost/MAMP/"

7. On this page select the 'phpMyAdmin' button

8. Select 'SQL' and paste all contents of localhost.sql into the text field

9. Press the 'go' button below the text field

10. Type "localhost" into the web address bar

11. Select 'index' from the list of folders within westbaywaratahs folder

12. Website should now work properly