SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `westbaywaratahs` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `westbaywaratahs`;


/********************** USER TABLES **********************/

/*admin table*/
/*admin is a user that can do all the administrator functions*/
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `F_NAME` varchar(20) NOT NULL,
  `L_NAME` varchar(20) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `U_TYPE` int(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE (ID, USERNAME, EMAIL)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*admin account for testing*/
/*INSERT INTO `tbl_admin` (`USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `EMAIL`, `U_TYPE`) VALUES
('admin', 'admin', 'ad', 'min', 'loganwalkernz@gmail.com', 1);*/

/*test data*/
INSERT INTO `tbl_admin` (`USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `EMAIL`, `U_TYPE`) VALUES
('andrea1', 'Ilovefoliage123', 'Andrea', 'Lett', 'andrealett123@gmail.com', 1);
INSERT INTO `tbl_admin` (`USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `EMAIL`, `U_TYPE`) VALUES
('jim1', 'Abcdef123', 'Jim', 'Lett', 'jimlett123@gmail.com', 1);

/*customer table*/
/*customer is a user that can do all the customer functions*/
DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE `tbl_customer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_NAME` varchar(50) NOT NULL,
  `DELIVERY_ADDRESS` varchar(500),
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `F_NAME` varchar(20) NOT NULL,
  `L_NAME` varchar(20) NOT NULL,
  `COUNTRY` varchar(50),
  `EMAIL` varchar(50) NOT NULL,
  `U_TYPE` int(1) NOT NULL,
  `CONFIRMED` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE (ID, USERNAME, EMAIL)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*customer account for testing*/
/*INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('flower shop', '997 Watson Road, China', 'customer', 'customer', 'cus', 'tomer', 'China', 'chinaflowers@gmail.com', 0, 'yes');*/

/*test data*/
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Quality Bouquets', '997 Watson Road', 'Bouquets12', 'coolcats55', 'Jim', 'Williams', 'New Zealand', 'qualitybouquets@gmail.com', 0, 'yes');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Flowers on 7th', '123 7th Avenue', 'Flowers7th', 'meandogs38', 'Rodger', 'Michaels', 'Australia', '7thflowers@gmail.com', 0, 'yes');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Nice Flower Shop', '573 Richard Street', 'Niceflowers33', 'nicecows77', 'Bill', 'Wilson', 'Canada', 'niceflowers@gmail.com', 0, 'yes');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('We Sell Flowers', '233 First Street', 'Flowerseller22', 'greatmoose787', 'Russel', 'Walker', 'Australia', 'wesellflowers@gmail.com', 0, 'yes');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Pine Leafs Shop', '997 Second Street', 'Pineleafs32', 'amazingmongoose22', 'Luke', 'Stock', 'Japan', 'pineleafs@gmail.com', 0, 'yes');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Medicinal Foliage', '273 Wilson Avenue', 'Medicinal55', 'excitingsmoothie8', 'Leanne', 'Anderson', 'Vietnam', 'medicalfoliage@gmail.com', 0, 'yes');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Foliage Distributors', '22 St Andrews Street', 'Fdistributor7', 'justintime2', 'Julia', 'Obrien', 'Canada', 'foliagedistributors@gmail.com', 0, 'no');
INSERT INTO `tbl_customer` (`COMPANY_NAME`, `DELIVERY_ADDRESS`, `USERNAME`, `PASSWORD`, `F_NAME`, `L_NAME`, `COUNTRY`, `EMAIL`, `U_TYPE`, `CONFIRMED`) VALUES
('Mean as Foliage', '664 Tay Street', 'Meanfoliage', 'crazyhorses7', 'Matt', 'Harris', 'New Zealand', 'meanfolaige@gmail.com', 0, 'no');


/*user table*/
/*for signing in, allows for one login function*/
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `CUSTOMER_ID` int(11) REFERENCES tbl_customer(ID),
  `ADMIN_ID` int(11) REFERENCES tbl_admin(ID),  
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `U_TYPE` int(1) NOT NULL,
  PRIMARY KEY (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `tbl_user` (`ADMIN_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES (50, 'admin', 'admin', 1);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES (50, 'customer', 'customer', 0);

/*test data*/
INSERT INTO `tbl_user` (`ADMIN_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('1', 'andrea1', 'Ilovefoliage123', 1);
INSERT INTO `tbl_user` (`ADMIN_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ( '2', 'jim1', 'Abcdef123', 1);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('1', 'Bouquets12', 'coolcats55', 0);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('2', 'Flowers7th', 'meandogs38', 0);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('3', 'Niceflowers33', 'nicecows77', 0);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('4', 'Flowerseller22', 'greatmoose787', 0);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('5', 'Pineleafs32', 'amazingmongoose22', 0);
INSERT INTO `tbl_user` (`CUSTOMER_ID`, `USERNAME`, `PASSWORD`, `U_TYPE`) VALUES ('6', 'Medicinal55', 'excitingsmoothie8', 0);

/******************** DELIVERY TABLES ********************/

/*products table*/
/*holds information about products (pricing, price per stem)*/
DROP TABLE IF EXISTS `tbl_products`;
CREATE TABLE `tbl_products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `IMAGE` varchar(500) NOT NULL,
  `COST` int(11), /*to be confirmed as to what the pricing will be*/
  `AVALIBILITY` varchar(100), /*will describe if it is availble or not*/
  PRIMARY KEY (`ID`),
  UNIQUE (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*test data, name is correct but pricing and avalibility will be changed*/
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Casuarina', '../images/foliage/Casuarina.jpg', 60, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Ceratopetalum Budded', '../images/foliage/CeratopetalumBudded.jpg', 80, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Confer Green', '../images/foliage/ConferGreen.jpg', 40, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Conifer Gold', '../images/foliage/ConiferGold.jpg', 35, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Cytisus', '../images/foliage/cytisus.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Feijoa', '../images/foliage/Feijoa.jpg', 45, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Fiesta', '../images/foliage/Fiesta.jpg', 60, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Hydrangea Budded', '../images/foliage/HydrangeaBudded.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Hydrangea Foliage', '../images/foliage/HydrangeaFoliage.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Laurel', '../images/foliage/Laurel.jpg', 40, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Lophomyrtus Kathryn', '../images/foliage/LophomyrtusKathryn.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Magnolia Grandiflora', '../images/foliage/MagnoliaGrandiflora.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Mel Lim', '../images/foliage/MelLim.jpg', 40, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Pittosporum Cassies Cream', '../images/foliage/PittoCassiesCream.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Pittosporum OB', '../images/foliage/PittoOB.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Pittosporum Tasman Ruffles', '../images/foliage/PittoTasmanRuffles.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Pittosporum Tenui', '../images/foliage/PittoTenui.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Pittosporum Irene Patterson', '../images/foliage/PittosporumIrenePatterson.jpg', 60, 'no');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Waratah Pink Hybrid', '../images/foliage/WaratahPinkHybrid.jpg', 60, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Waratahs', '../images/foliage/Waratahs.jpg', 45, 'yes');
INSERT INTO `tbl_products` (`NAME`, `IMAGE`, `COST`, `AVALIBILITY`) VALUES ('Black Boater', '../images/foliage/blackboater.jpg', 60, 'yes');

/*product ordered table*/
/*tells the order what has been ordered*/
DROP TABLE IF EXISTS `tbl_product_ordered`;
CREATE TABLE `tbl_product_ordered` (
  `ORDER_ID` int(11) REFERENCES tbl_order(ID),
  `ITEM` varchar(100) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `COST` int(11) REFERENCES tbl_products(COST)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*test data*/
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Pittosporum', 10, 400);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Conifer Gold', 5, 175);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Feijoa', 5, 225);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Fiesta', 5, 300);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (2, 'Laurel', 20, 800);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (2, 'Mel Lim', 5, 200);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (2, 'Casuarina', 5, 300);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (3, 'Feijoa', 20, 900);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (3, 'Fiesta', 20, 1200);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (4, 'Pittosporum', 50, 2000);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (5, 'Conifer Gold', 30, 1050);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (5, 'Mel Lim', 10, 400);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (5, 'Confer Green', 10, 400);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Pittosporum', 30, 1200);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Conifer Gold', 30, 1050);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Feijoa', 20, 900);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Fiesta', 10, 600);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (7, 'Feijoa', 20, 900);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (7, 'Fiesta', 20, 1200);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Pittosporum', 10, 400);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Conifer Gold', 5, 175);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Feijoa', 5, 225);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Fiesta', 5, 300);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (9, 'Feijoa', 20, 900);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (9, 'Fiesta', 20, 1200);

INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (10, 'Pittosporum', 10, 400);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (10, 'Conifer Gold', 5, 175);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (10, 'Feijoa', 5, 225);
INSERT INTO `tbl_product_ordered` (`ORDER_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (10, 'Fiesta', 5, 300);

/*order table*/
/*created when a customer places an order*/
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(11) REFERENCES tbl_customer(ID),
  `ORDER_DATE` date NOT NULL,
  `DELIVERY_DATE` date NOT NULL,
  `STATUS` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*test data*/
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(6, '2017-03-26', '2017-04-14', 'Paid');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(3, '2017-04-15', '2017-04-30', 'Invoice Created');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(2, '2017-05-02', '2017-05-19', 'Shipped');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(4, '2017-05-23', '2017-06-12', 'Shipped');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(5, '2017-06-12', '2017-06-29', 'Shipped');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(1, '2017-07-02', '2017-07-22', 'Shipped');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(2, '2017-07-24', '2017-08-14', 'Shipped');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(6, '2017-08-13', '2017-08-30', 'Shipped');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(2, '2017-09-02', '2017-09-29', 'Ordered');
INSERT INTO `tbl_order` (`CUSTOMER_ID`, `ORDER_DATE`, `DELIVERY_DATE`, `STATUS`) VALUES
(6, '2017-09-12', '2017-10-02', 'Ordered');

/*product shipped table*/
/*tells the invoice what has been shipped*/
DROP TABLE IF EXISTS `tbl_product_shipped`;
CREATE TABLE `tbl_product_shipped` (
  `INVOICE_ID` int(11) REFERENCES tbl_invoice(ID),
  `ITEM` varchar(100) NOT NULL,
  `QUANTITY` int(11) NOT NULL,
  `COST` int(11) REFERENCES tbl_products(COST)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Pittosprorum', 10, 40);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Conifer Gold', 5, 35);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (1, 'Fiesta', 5, 60);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (2, 'Laurel', 15, 40);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (2, 'Mel Lim', 5, 40);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (2, 'Casuarina', 5, 60);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (3, 'Feijoa', 20, 45);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (3, 'Fiesta', 20, 60);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (4, 'Pittosprorum', 40, 40);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (5, 'Conifer Gold', 30, 35);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (5, 'Mel Lim', 10, 40);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (5, 'Confer Green', 10, 40);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Pittosprorum', 25, 40);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Conifer Gold', 30, 35);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (6, 'Feijoa', 20, 45);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (7, 'Feijoa', 20, 45);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (7, 'Fiesta', 20, 60);

INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Pittosprorum', 10, 40);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Conifer Gold', 5, 35);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Feijoa', 5, 45);
INSERT INTO `tbl_product_shipped` (`INVOICE_ID`, `ITEM`, `QUANTITY`, `COST`) VALUES (8, 'Fiesta', 5, 60);

/*invoice table*/
/*created when an order is shipped, has final pricing on it*/
DROP TABLE IF EXISTS `tbl_invoice`;
CREATE TABLE `tbl_invoice` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) REFERENCES tbl_orders(ID),
  `CUSTOMER_ID` int(11) NOT NULL,
  `DELIVERY_ADDRESS` varchar(50) NOT NULL,
  `ORDER_DATE` date NOT NULL,
  `SHIPMENT_DATE` date NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*test data*/
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(1, 1, '997 Watson Road', '2017-03-26', '2017-04-14');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(2, 3, '573 Richard Street', '2017-04-15', '2017-04-30');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(3, 2, '123 7th Avenue', '2017-05-02', '2017-05-19');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(4, 4, '233 First Street', '2017-05-23', '2017-06-12');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(5, 5, '997 Second Street', '2017-06-12', '2017-06-29');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(6, 1, '997 Watson Road', '2017-07-02', '2017-07-22');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(7, 2, '123 7th Avenue', '2017-07-24', '2017-08-14');
INSERT INTO `tbl_invoice` (`ORDER_ID`, `CUSTOMER_ID`, `DELIVERY_ADDRESS`, `ORDER_DATE`, `SHIPMENT_DATE`) VALUES
(8, 6, '273 Wilson Avenue', '2017-08-13', '2017-08-30');

/******************** OTHER TABLES ********************/

/*property maintenance table*/
/*holds all information about the properties of westbay*/
DROP TABLE IF EXISTS `tbl_properties`;
CREATE TABLE `tbl_properties` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `IMAGE_LINK` varchar(500) NOT NULL,
  `MOWN_DATE` varchar(50) NOT NULL,
  `SPRAYED_DATE` varchar(50) NOT NULL,
  `COMMENTS` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_properties` (`NAME`, `IMAGE_LINK`, `MOWN_DATE`, `SPRAYED_DATE`, `COMMENTS`) VALUES 
('Pine Ridge Lane', '../images/WestbayHome1.jpg', '08/09/2017', '10/09/2017', 'A bit untidy around the edges at the moment');

INSERT INTO `tbl_properties` (`NAME`, `IMAGE_LINK`, `MOWN_DATE`, `SPRAYED_DATE`, `COMMENTS`) VALUES 
('Beach Road', '../images/aboutimg2.jpg', '08/09/2017', '10/09/2017', 'This property is overrun by gnomes.');

INSERT INTO `tbl_properties` (`NAME`, `IMAGE_LINK`, `MOWN_DATE`, `SPRAYED_DATE`, `COMMENTS`) VALUES 
('Killen Road', '../images/aboutimg.jpg', '08/09/2017', '10/09/2017', 'Hedge needs trimming ASAP');

/* Nav bar table */
/* Navigation bar loads from this table for different users */
DROP TABLE IF EXISTS `tbl_nav`;
CREATE TABLE `tbl_nav` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_TYPE` int(11) REFERENCES tbl_user(U_TYPE),
  `LINK` varchar(256) NOT NULL,
  `TITLE` varchar(256) NOT NULL,
  `NAME` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (1, '../index/index.php', 'Home', 'index');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (1, '../admin/property.php', 'Maintain Property', 'maintain_property');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (1, '../admin/updateproducts.php', 'Update Products', 'update_products');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (1, '../admin/confirmcustomer.php', 'Confirm Customer', 'confirm_customer');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (1, '../admin/currentorders.php', 'Orders', 'current_ordersadmin');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (1, '../admin/invoices.php', 'Invoices', 'invoice_search');


INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (0, '../index/index.php', 'Home', 'index');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (0, '../index/products.php', 'Products', 'products');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (0, '../customer/makeorder.php', 'Make an order', 'make_order');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (0, '../customer/currentorders.php', 'Current orders', 'current_orders');
INSERT INTO `tbl_nav` (`USER_TYPE`, `LINK`, `TITLE`, `NAME`) VALUES (0, '../customer/orderhistory.php', 'Order history', 'order_history');