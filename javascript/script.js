/* #####################################################################
   #
   #   Project       : Modal Login with jQuery Effects
   #   Author        : Rodrigo Amarante (rodrigockamarante)
   #   Version       : 1.0
   #   Created       : 07/29/2015
   #   Last Change   : 08/04/2015
   #
   ##################################################################### */
   


function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(document).ready(function() {
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    $("#deliveryDate").datepicker({ dateFormat: 'yy-mm-dd'}).datepicker("setDate", 15);
    $("#datepicker1").datepicker();
    $("#datepickertwo1").datepicker();
    $("#datepicker2").datepicker();
    $("#datepickertwo2").datepicker();
    $("#datepicker3").datepicker();
    $("#datepickertwo3").datepicker();
    $("#datepicker4").datepicker();
    $("#datepickertwo4").datepicker();
    $("#datepicker5").datepicker();
    $("#datepickertwo5").datepicker();
    $("#datepicker6").datepicker();
    $("#datepickertwo6").datepicker();
    $("#datepicker7").datepicker();
    $("#datepickertwo7").datepicker();
  });
