<?php   include_once('../functions/functions.php');
        include_once('../functions/functionshome.php');
        include_once('../functions/functionsnav.php');
        session_start();
        loginUser(); 
        registerCustomer();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Westbay Waratahs</title>
        <link rel="icon" href="../images/imgtab.png">

        <!-- Bootstrap stylesheets -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="../css/styles.css" type="text/css">
        
        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="validation.min.js"></script>
        <script type="text/javascript" src="../javascript/script.js"></script>
    </head>
    <body>
        
        <!-- _________________________________navigation bar code starts here________________________ -->
        <!-- LOGIN/LOGIN CHECK -->

        <?php
        if(isset($_SESSION['login']))
        {
        ?>

        <!-- Nav shown to logged in user-->
        <nav class="navbar navbar-inverse" id="grad-nav">
            <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li><a href="../index/index.php">Home</a></li>
                    <?php echo show_contentNav(get_all_nav());?>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">   <span class="glyphicon glyphicon-log-in">  <?php echo $_SESSION['user'];?></span><span class="caret"></span></a>
                        <ul class="dropdown-menu ">
                            <!-- <li><a href="../customer/account.php">Account settings</a></li> -->
                            <li><a href="../functions/logout.php">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <?php
        }
        else
        {
        ?>

        <!-- Nav shown to not logged in user -->
        <nav class="navbar navbar-inverse" id="grad-nav">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-right">
                    <li><button type="button" name="login" id="login" class="btn btn-success" data-toggle="modal" data-target="#loginModal">Login</button>
                    <!--<a href="#" role="button" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
                </ul>
            </div>
        </nav>

        <?php
        }
        ?>
        
        <!-- login modal here -->
        <div class="modal fade" id="loginModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header" align="center">
                        <img class="img-circle" id="img_logo" src="../images/logo.png">
                        <button type"button" class="close" data-dismiss="modal">&times;</button>
                        <h2> Login </h2>
                    </div>

                    <div id="div-forms">

                        <?php 
                        if(!@$_SESSION['login']) 
                        {
                        ?>

                        <form role="form" method="post" name="Login_Form" class="form-signin" id="login-form">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" id="login_username" name="Username" class="form-control" placeholder="username" required="" autofocus=""/>
                                    <input type="password" id="login_password" name="Password" class="form-control" placeholder="password" required=""/>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-primary btn-block" name="login" value="Login" type="Submit">Log in </button>
                                <button class="btn btn-info btn-block" name="register" value="Resgister" formaction="register.php"type="submit">Not a Customer? Register Here!</button>
                            </div>
                        </form>

                        <?php
                        }
                        else
                        {
                        ?>
                            you are logged in
                        <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
        
        <!-- jumbotron code starts here -->
        <div class="container">
            <div class="jumbotron">
                <img src="../images/border.jpg" width="">
            </div>   
        </div>

        <!-- Main content starts here -->
        <div class="container">
            <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'About')" id="defaultOpen">About</button>
                <button class="tablinks" onclick="openCity(event, 'Products')">Products</button>
                <button class="tablinks" onclick="openCity(event, 'Contact')">Contact</button>
            </div>

            <div id="About" class="tabcontent">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">About</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Welcome to Westbay Waratahs, the southern hemispheres largest exporter of foliage<br>
                           We have been exporting foliage since 1967, out of the Western Bay of Plenty</p>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="products-padding">
                            <img src="../images/WestbayHome1.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="products-padding">
                            <img src="../images/WestbayHome2.jpg">
                        </div>
                    </div>
                </div>
            </div>

            <div id="Products" class="tabcontent">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Our Products</h1>
                    </div>
                </div>
                <div class="row">
                    <?php echo show_productsHome(get_productsHome());?>
                </div>
            </div>  
            <div id="Contact" class="tabcontent">
                <h3>Contact</h3>
                <p>Couldn't care less right now. Ya yet.</p>
            </div>
        </div>
        <script>
            // Get the element with id="defaultOpen" and click on it
            document.getElementById("defaultOpen").click();
        </script>
        
    </body>
</html>