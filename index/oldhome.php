<?php   include_once('../functions/functions.php');
        include_once('../functions/functionshome.php');
        include_once('../functions/functionsnav.php');
        session_start();
        loginUser(); 
        registerCustomer();
        pushID();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Westbay Waratahs</title>
        <link rel="icon" href="../images/imgtab.png">

        <!-- Bootstrap stylesheets -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="../css/styles.css" type="text/css">
        
        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="validation.min.js"></script>
        <script type="text/javascript" src="../javascript/script.js"></script>
        <script type="text/javascript" src="../javascript/functions.js"></script>
    </head>
    <body>
        
        <!-- _________________________________navigation bar code starts here________________________ -->
        <!-- LOGIN/LOGIN CHECK -->

        <?php
        if(isset($_SESSION['login']))
        {
        ?>

        <!-- Nav shown to logged in user-->
        <nav class="navbar navbar-inverse" id="grad-nav">
            <div class="container-fluid">
                <form role="form" method="post" class="pushid" id="pushid">
                    <input type="hidden" name="id" value="<?php echo $_SESSION['id'];?>">
                    <ul class="nav navbar-nav">
                            <?php echo show_contentNav(get_all_nav());?>
                    </ul>
                </form>
                

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle btn btn-lg btn-login" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>  <?php echo $_SESSION['user'];?>  <span class="caret"></span></a>
                        <ul class="dropdown-menu ">
                            <li><a href="../customer/account.php">Account settings</a></li>
                            <li><a href="../functions/logout.php">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <?php
        }
        else
        {
        ?>

        <!-- Nav shown to not logged in user -->
        <nav class="navbar navbar-inverse" id="grad-nav">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-right">
                    <li><button type="button" name="login" id="login" class="btn btn-lg btn-login" data-toggle="modal" data-target="#loginModal"><span class="glyphicon glyphicon-user"></span> Login</button></li>
                    <!--<a href="#" role="button" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
                </ul>
            </div>
        </nav>

        <?php
        }
        ?>
        
        <!-- login modal here -->
        <div class="modal fade" id="loginModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header" align="center">
                        <img class="img-circle" id="img_logo" src="../images/logo.png">
                        <button type"button" class="close" data-dismiss="modal">&times;</button>
                        <h2> Login </h2>
                    </div>

                    <div id="div-forms">

                        <?php 
                        if(!@$_SESSION['login']) 
                        {
                        ?>

                        <form role="form" method="post" name="Login_Form" class="form-signin" id="login-form">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" id="login_username" name="Username" class="form-control" placeholder="username" required="" autofocus=""/>
                                    <input type="password" id="login_password" name="Password" class="form-control" placeholder="password" required=""/>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-primary btn-block" name="login" value="Login" type="Submit">Log in </button>
                                <button type="button" name="register" id="register" class="btn btn-success btn-block" data-toggle="modal" data-dismiss="modal" data-target="#registerModal">Not a Customer? Register Here!</button>
                            </div>
                        </form>

                        <?php
                        }
                        else
                        {
                        ?>
                            you are logged in
                        <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="registerModal" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header" align="center">
                        <img class="img-circle" id="img_logo" src="../images/logo.png">
                        <button type"button" class="close" data-dismiss="modal">&times;</button>
                        <h2> Register </h2>
                    </div>

                    <div id="div-forms">
                        <form role="form" method="post" name="Login_Form" class="form-signin" id="login-form">
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="text" id="register_username" name="Username" class="form-control" minlength="6" maxlength="14" placeholder="Username" required="" autofocus=""/>
                                    <input type="text" id="register_password" name="Company" class="form-control" placeholder="Company Name" required=""/>
                                    <input type="password" id="register_password" name="Password" class="form-control" minlength="6" maxlength="14" placeholder="Password" required=""/>
                                    <input type="text" id="register_fname" name="Fname" class="form-control" placeholder="First Name" required=""/>
                                    <input type="text" id="register_lname" name="Lname" class="form-control" placeholder="Last Name" required=""/>
                                    <select type="text" id="register_country" name="Country" class="form-control">
                                        <option value="Afghanistan">Afghanistan</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option>
                                        <option value="American Samoa">American Samoa</option><option value="Andorra">Andorra</option><option value="Angola">Angola</option>
                                        <option value="Anguilla">Anguilla</option><option value="Antartica">Antarctica</option><option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                        <option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Aruba">Aruba</option>
                                        <option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option>
                                        <option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option>
                                        <option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option>                                        <option value="Belize">Belize</option>
                                        <option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option>
                                        <option value="Bolivia">Bolivia</option><option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option><option value="Botswana">Botswana</option>
                                        <option value="Bouvet Island">Bouvet Island</option> <option value="Brazil">Brazil</option><option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                        <option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option>
                                        <option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option> <option value="Canada">Canada</option>
                                        <option value="Cape Verde">Cape Verde</option>  <option value="Cayman Islands">Cayman Islands</option> <option value="Central African Republic">Central African Republic</option>
                                        <option value="Chad">Chad</option> <option value="Chile">Chile</option> <option value="China">China</option>
                                        <option value="Christmas Island">Christmas Island</option> <option value="Cocos Islands">Cocos (Keeling) Islands</option><option value="Colombia">Colombia</option>
                                        <option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="Congo">Congo, the Democratic Republic of the</option>
                                        <option value="Cook Islands">Cook Islands</option> <option value="Costa Rica">Costa Rica</option><option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                        <option value="Croatia">Croatia (Hrvatska)</option> <option value="Cuba">Cuba</option> <option value="Cyprus">Cyprus</option>
                                        <option value="Czech Republic">Czech Republic</option> <option value="Denmark">Denmark</option>  <option value="Djibouti">Djibouti</option>
                                        <option value="Dominica">Dominica</option><option value="Dominican Republic">Dominican Republic</option>   <option value="East Timor">East Timor</option>
                                        <option value="Ecuador">Ecuador</option> <option value="Egypt">Egypt</option> <option value="El Salvador">El Salvador</option>
                                        <option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option>
                                        <option value="Ethiopia">Ethiopia</option> <option value="Falkland Islands">Falkland Islands (Malvinas)</option> <option value="Faroe Islands">Faroe Islands</option>
                                        <option value="Fiji">Fiji</option>  <option value="Finland">Finland</option> <option value="France">France</option>
                                        <option value="France Metropolitan">France, Metropolitan</option> <option value="French Guiana">French Guiana</option> <option value="French Polynesia">French Polynesia</option>
                                        <option value="French Southern Territories">French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option>
                                        <option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option>
                                        <option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option> <option value="Greenland">Greenland</option>
                                        <option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option> <option value="Guam">Guam</option> <option value="Guatemala">Guatemala</option>
                                        <option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option>
                                        <option value="Haiti">Haiti</option><option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option><option value="Holy See">Holy See (Vatican City State)</option>
                                        <option value="Honduras">Honduras</option> <option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option>
                                        <option value="Iceland">Iceland</option><option value="India">India</option> <option value="Indonesia">Indonesia</option>
                                        <option value="Iran">Iran (Islamic Republic of)</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option>
                                        <option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option>
                                        <option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option>
                                        <option value="Kiribati">Kiribati</option><option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                                        <option value="Korea">Korea, Republic of</option><option value="Kuwait">Kuwait</option> <option value="Kyrgyzstan">Kyrgyzstan</option>
                                        <option value="Lao">Lao People's Democratic Republic</option> <option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option>
                                        <option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option>
                                        <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> <option value="Liechtenstein">Liechtenstein</option>
                                        <option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macau">Macau</option>
                                        <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option> <option value="Madagascar">Madagascar</option>
                                        <option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option>
                                        <option value="Mali">Mali</option><option value="Malta">Malta</option> <option value="Marshall Islands">Marshall Islands</option>
                                        <option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option>
                                        <option value="Mexico">Mexico</option><option value="Micronesia">Micronesia, Federated States of</option><option value="Moldova">Moldova, Republic of</option><option value="Monaco">Monaco</option>
                                        <option value="Mongolia">Mongolia</option><option value="Montserrat">Montserrat</option>
                                        <option value="Morocco">Morocco</option> <option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option>  <option value="Namibia">Namibia</option>
                                        <option value="Nauru">Nauru</option> <option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option>
                                        <option value="Netherlands Antilles">Netherlands Antilles</option><option value="New Caledonia">New Caledonia</option> <option value="New Zealand"selected>New Zealand</option>
                                        <option value="Nicaragua">Nicaragua</option> <option value="Niger">Niger</option>
                                        <option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Norway">Norway</option>
                                        <option value="Oman">Oman</option><option value="Pakistan">Pakistan</option> <option value="Palau">Palau</option><option value="Panama">Panama</option>
                                        <option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option>
                                        <option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option>
                                        <option value="Qatar">Qatar</option> <option value="Reunion">Reunion</option> <option value="Romania">Romania</option> <option value="Russia">Russian Federation</option>
                                        <option value="Rwanda">Rwanda</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint LUCIA">Saint LUCIA</option><option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                                        <option value="Samoa">Samoa</option> <option value="San Marino">San Marino</option><option value="Sao Tome and Principe">Sao Tome and Principe</option> <option value="Saudi Arabia">Saudi Arabia</option>
                                        <option value="Senegal">Senegal</option><option value="Seychelles">Seychelles</option> <option value="Sierra">Sierra Leone</option><option value="Singapore">Singapore</option>
                                        <option value="Slovakia">Slovakia (Slovak Republic)</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option>
                                        <option value="South Africa">South Africa</option> <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                                        <option value="Span">Spain</option> <option value="SriLanka">Sri Lanka</option> <option value="St. Helena">St. Helena</option><option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                                        <option value="Sudan">Sudan</option><option value="Suriname">Suriname</option> <option value="Svalbard">Svalbard and Jan Mayen Islands</option> <option value="Swaziland">Swaziland</option>
                                        <option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option> <option value="Syria">Syrian Arab Republic</option><option value="Taiwan">Taiwan, Province of China</option>
                                        <option value="Tajikistan">Tajikistan</option><option value="Tanzania">Tanzania, United Republic of</option><option value="Thailand">Thailand</option><option value="Togo">Togo</option>
                                        <option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option>
                                        <option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks and Caicos">Turks and Caicos Islands</option> <option value="Tuvalu">Tuvalu</option>
                                        <option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option>
                                        <option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option><option value="Uruguay">Uruguay</option>
                                        <option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela">Venezuela</option> <option value="Vietnam">Viet Nam</option>
                                        <option value="Virgin Islands (British)">Virgin Islands (British)</option><option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                        <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option><option value="Western Sahara">Western Sahara</option>
                                        <option value="Yemen">Yemen</option><option value="Yugoslavia">Yugoslavia</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option>
                                    </select>
                                    <input type="email" id="register_email" name="Email" class="form-control" placeholder="Email Address" required=""/>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-primary btn-block" name="register" value="Register" type="Submit">Register</button>
                                <button type="button" name="register" id="register" class="btn btn-success btn-block" data-toggle="modal" data-dismiss="modal" data-target="#loginModal">Back to Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- jumbotron code starts here -->
        <div class="container">
            <div class="jumbotron">
                <img src="../images/border.jpg" width="">
            </div>   
        </div>

        <!-- Main content starts here -->
        <div class="container">
            <div class="tab">
                <button class="tablinks" onclick="openCity(event, 'Home')" id="defaultOpen">Home</button>
                <button class="tablinks" onclick="openCity(event, 'Products')">Products</button>
                <button class="tablinks" onclick="openCity(event, 'About')">About</button>
                <button class="tablinks" onclick="openCity(event, 'Contact')">Contact</button>
            </div>
            
        <div id="Home" class="tabcontent">
               <!-- <div class="row">
                    <div class="col-md-12">
                       <!-- <h1 class="page-header">About Us</h1> 
                    </div>
                </div>-->

                <div class="row">
                    <div class="col-md-12">
                        <div class="products-padding">
                            <h1>Welcome to Westbay Waratahs!</h1>
                            <br>
                            <blockquote class="blockquote">
                                <p class="mb-0">Welcome to Westbay Waratahs, the largest commerical flower and foliage cutters in
                                    southern hemisphere!        
                                <br>

                                </p>
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                     <!-- Indicators -->
                       <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                        <img src="../images/slider/slider2.png" alt="Westbay Waratahs">
                                        </div>

                                        <div class="item">
                                        <img src="../images/slider/slider1.png" alt="Westbay Waratahs">
                                        </div>

                                        <div class="item">
                                        <img src="../images/slider/slider3.png" alt="Westbay Waratahs">
                                        </div>
                                    </div>

                                <!-- Left and right controls 
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                  <!--  <span class="glyphicon glyphicon-chevron-left"></span> -->
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                   <!-- <span class="glyphicon glyphicon-chevron-right"></span> -->
                                    <span class="sr-only">Next</span>
                                </a>  -->
                            </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="products-padding">
                            <h1>Who are we?</h1>
                            <br>
                            <blockquote class="blockquote">
                                <p class="mb-0">Westbay Waratahs is a small family owned and operated business established in 1976 in Katikati, 
                                New Zealand by the two founding members, Gillian and Graham Lett. Gillian and Graham previous were dairy farmers,
                                 but when they became tired of dairy farming, they began growing flowers as it was something they both had a strong passion for. 
                                <br>
                                <br>
                                Westbay Waratahs started over 40 years ago with a half dozen types of flowers and foliage. 
                                Since then, Westbay Waratahs now operate on three different properties in Katikati, 
                                New Zealand and now globally export over 25 different varieties of flowers and foliage. 
                                </p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>

            <div id="About" class="tabcontent">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">About Us</h1> 
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="products-padding">
                            <h1>Who are we?</h1>
                            <br>
                            <blockquote class="blockquote">
                                <p class="mb-0">Westbay Waratahs is a small family owned and operated business established in 1976 in Katikati, 
                                New Zealand by the two founding members, Gillian and Graham Lett. Gillian and Graham previous were dairy farmers,
                                 but when they became tired of dairy farming, they began growing flowers as it was something they both had a strong passion for. 
                                <br>
                                <br>
                                Westbay Waratahs started over 40 years ago with a half dozen types of flowers and foliage. 
                                Since then, Westbay Waratahs now operate on three different properties in Katikati, 
                                New Zealand and now globally export over 25 different varieties of flowers and foliage. 
                                </p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="products-padding">
                            <img src="../images/aboutimg2.jpg">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="products-padding">
                            <img src="../images/aboutimg.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="products-padding">
                            <h1>What do we do?</h1>
                            <br>
                           <blockquote class="blockquote">
                                <p class="mb-0">Westbay Waratahs produce over 25 different varieties of flowers and foliage and export them around the world. 
                                Westbay Waratahs are the largest commercial exporters of cut flowers and foliage in the southern hemisphere. 
                                </p>
                            </blockquote>
                        </div>
                    </div>
                </div>

            </div>

            <!-- 
                 <div id="About" class="tabcontent">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">About</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Welcome to Westbay Waratahs, the southern hemispheres largest exporter of foliage<br>
                           We have been exporting foliage since 1967, out of the Western Bay of Plenty</p>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="products-padding">
                            <img src="../images/WestbayHome1.jpg">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="products-padding">
                            <img src="../images/WestbayHome2.jpg">
                        </div>
                    </div>
                </div>
            </div>
            -->

            <div id="Products" class="tabcontent">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Our Products</h1>
                            <blockquote class="blockquote">
                                <p class="mb-0">Westbay Waratahs produce a wide variety of flowers and foliage, 
                                    scrolling down this page you will be able to see all of the flowers and foliage we produce, 
                                    and information of their availability. 
                                <br>
                                </p>
                            </blockquote>
                    </div>
                </div>
                <div class="row">
                    <?php echo show_productsHome(get_productsHome());?>
                </div>
            </div>  
            <div id="Contact" class="tabcontent">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Contact</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <h3>Address</h3>
                        <p>84 Pine Ridge Lane</p>
                        <p>R D 4 Katikati 3181</p>
                        <p>Bay of Plenty, New Zealand</p>
                        <br>
                    </div>
                    <div class="col-md-4">
                        <h3>Contact Details</h3>
                        <p>Telephone: 07-552-0896</p>
                        <p>Fax: 07-552-0933</p>
                        <p>Email: info@westbaywaratahs.co.nz</p>
                        <br>
                    </div>
                    <div class="col-md-4">
                        <h3>Social media</h3>
                        <a href="https://www.facebook.com/WestbayWaratahsLimited/" style="text-decoration: none !important;">
                            <img src="../images/faceberk.png" style="width:42px;height:42px;border:0;">
                        </a>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <script>
            // Get the element with id="defaultOpen" and click on it
            document.getElementById("defaultOpen").click();
        </script>
        
    </body>
</html>