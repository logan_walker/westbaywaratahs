<?php

    
    include_once('creds.php');

    if(@$_POST['form_submitted']) {

        if(@$_POST['button1'] == "updateProduct") {
            updateProduct();
        }
        else if(@$_POST['button1'] == "deleteProduct") {
            deleteProduct();
        }
        else if(@$_POST['button1'] == "addProduct") {
            addProduct();
        }

    } 

    function get_productInformation() {
            
            $db = connection();
            $sql = "SELECT * FROM tbl_products ORDER BY name";
            $arr = [];

            $result = $db->query($sql);
        
            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }

            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "id" => $row['ID'],
                    "name" => $row['NAME'],
                    "image" => $row['IMAGE'],
                    "cost" => $row['COST'],
                    "avalibility" => $row['AVALIBILITY']
                );
            }

            $json = json_encode($arr);
        
            $result->free();
            $db->close();
            
            return $json;
        }

        function show_productInformation($data) {
            $array = json_decode($data, True);
            $output = "";
            $currentstatus = "";

            
            if (count($array) > 0 ) {
                for ($i = 0; $i < count($array); $i++) {
                    

                    if ($array[$i]['avalibility'] == "no") {
                        
                       $currentstatus = "<div class=\"radio\">
                                            <label><input type=\"radio\" name=\"avalibility\" value=\"yes\">In Season</label>
                                            <label><input type=\"radio\" name=\"avalibility\" value=\"no\" checked=\"checked\">Out of Season</label>
                                        </div>"; 
                    } 
                    else if ($array[$i]['avalibility'] == "yes") {

                        $currentstatus = "<div class=\"radio\">
                                            <label><input type=\"radio\" name=\"avalibility\" value=\"yes\" checked=\"checked\">In Season</label>
                                            <label><input type=\"radio\" name=\"avalibility\" value=\"no\">Out of Season</label>
                                        </div>";
                    } 

                    $output .= "<tr>
                                    <form method=\"POST\">
                                        <input type =\"hidden\" name=\"form_submitted\" value=\"1\">
                                        <input type=\"hidden\" name=\"id\" value=\"".$array[$i]['id']."\">
                                        <th>    
                                            <h5>Product Image</h5>
                                                <img class=\"updateproductspage\" src=\"".$array[$i]['image']."\">
                                        </th>
                                        <td> 
                                                <h5>Image Link</h5>
                                                <input type=\"text\" name=\"image\" value=\"".$array[$i]['image']."\">
                                        </td>
                                        <td><h5>Product Name</h5><input class=\"updateproductscenter\" type=\"text\" name=\"name\" value=\"".$array[$i]['name']."\"></td>
                                        <td><h5>Cost (p/b excl. GST)</h5><input class=\"updateproductscenter\" type=\"text\" name=\"cost\" value=\"".$array[$i]['cost']."\"></td>
                                        <td><h5>Avalibility</h5>$currentstatus</td>
                                        <td>
                                            <h5>Update</h5>
                                            <button class=\"updateproductscenterbutton\" type=\"submit\" name=\"button1\" value=\"updateProduct\">Update</button>
                                            <button class=\"updateproductscenterbutton\" type=\"submit\" name=\"button1\" value=\"deleteProduct\" <a href=\"deletelink\" onclick=\"return confirm('Are you sure you want to delete ".$array[$i]['name']." ?')\"></a>Delete</button>
                                        </td>
                                    </form>
                                </tr>";
                }
                return $output;
            }

            else {
                $output .= "<tr><td colspan='5'>No pending customers</td></tr>";
                return $output;
            }
        }

        function show_addButton() {
            $addbutton = "";

            $addbutton .= " <form method=\"POST\">
                                <input type =\"hidden\" name=\"form_submitted\" value=\"1\">
                                <tr>
                                <button class=\"addproductbutton\" type=\"submit\" name=\"button1\" value=\"addProduct\">Add New Product</button>
                                </tr>
                            </form>";
            


            return $addbutton;
            exit();
        }

        

        function updateProduct() {        
            if(@$_POST['button1']=="updateProduct") {
                $db = connection();

                $image = $db->real_escape_string($_POST['image']);
                $name = $db->real_escape_string($_POST['name']);
                $cost = $db->real_escape_string($_POST['cost']);
                $avalibility = $db->real_escape_string($_POST['avalibility']);
                $id = $db->real_escape_string($_POST['id']);

                $sql = "UPDATE tbl_products SET IMAGE='".$image."', NAME='".$name."', COST='".$cost."', AVALIBILITY='".$avalibility."' WHERE ID = ".$id."";

                $result = $db->query($sql);

                $db->close();

                if ($result == 1) {
                    redirect("updateproducts.php");
                }
                else {
                    print_r($sql);
                    return "<br><br>An Error has occured";
                    exit();
                }
            }  
        }

        function deleteProduct() {        
            if(@$_POST['button1']=="deleteProduct") {
                $db = connection();


                $id = $db->real_escape_string($_POST['id']);

                $sql = "DELETE FROM tbl_products WHERE ID = $id";

                $result = $db->query($sql);

                $db->close();

                if ($result == 0) {
                    redirect("updateproducts.php");
                }
                else {
                    return "<br><br>An Error has occured";
                    exit();
                }
            }  
        }

        function addProduct() {        
            if(@$_POST['button1'] == "addProduct") {
                $db = connection();

               /* $image = $db->real_escape_string($_POST['image']);
                $name = $db->real_escape_string($_POST['name']);
                $cost = $db->real_escape_string($_POST['cost']);
                $avalibility = $db->real_escape_string($_POST['avalibility']);  */

               // $id = $db->real_escape_string($_POST['id']);

                $sql = ("INSERT INTO tbl_products (NAME, IMAGE, COST, AVALIBILITY) VALUES ('A Product', '../images/foliage/unavil.png', '$0', 'no')");
                $result = $db->query($sql);

                $db->close();

                if ($result == 0) {
                    redirect("updateproducts.php");
                }
                else {
                    return "<br><br>An Error has occured";
                    exit();
                }
            }  
        } 

   /* function addProduct(){
        if(isset($_POST['addProduct'])) {
            $db = connection();

            $name = $db->real_escape_string($_POST['name']);

            $stmt = $db->prepare("INSERT INTO tbl_products (NAME, IMAGE, COST, AVALIBILITY) VALUES ('Black Boater', '../images/foliage/blackboater.jpg', '$60', 'yes')");
            $stmt->bind_param("i", $content);
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("updateproducts.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    } */
    
?>