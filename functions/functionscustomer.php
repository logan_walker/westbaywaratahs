<?php

    
    include_once('creds.php');

    function get_unconfirmedCusomters() {
            
            $db = connection();
            $sql = "SELECT * FROM tbl_customer WHERE CONFIRMED='no'";
            $arr = [];

            $result = $db->query($sql);
        
            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }

            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "id" => $row['ID'],
                    "company_name" => $row['COMPANY_NAME'],
                    "delivery_address" => $row['DELIVERY_ADDRESS'],
                    "username" => $row['USERNAME'],
                    "password" => $row['PASSWORD'],
                    "f_name" => $row['F_NAME'],
                    "l_name" => $row['L_NAME'],
                    "country" => $row['COUNTRY'],
                    "email" => $row['EMAIL'],
                    "u_type" => $row['U_TYPE'],
                    "confirmed" => $row['CONFIRMED']
                );
            }

            $json = json_encode($arr);
        
            $result->free();
            $db->close();
            
            return $json;
        }

        function show_unconfirmedCustomers($data) {
            $array = json_decode($data, True);
            $output = "";

            if (count($array) > 0 ) {
                for ($i = 0; $i < count($array); $i++) {
                    $output .= "<tr>
                                    <th>".$array[$i]['id']."</th>
                                    <td>".$array[$i]['company_name']."</td>
                                    <td>".$array[$i]['username']."</td>
                                    <td>".$array[$i]['f_name']."</td>
                                    <td>".$array[$i]['l_name']."</td>
                                    <td>".$array[$i]['country']."</td>
                                    <td>".$array[$i]['email']."</td>
                                    <form method=\"POST\">
                                        <input type=\"hidden\" name=\"id\" value=\"".$array[$i]['id']."\">  
                                        <input type=\"hidden\" name=\"username\" value=\"".$array[$i]['username']."\">  
                                        <input type=\"hidden\" name=\"password\" value=\"".$array[$i]['password']."\">  
                                        <td><button type=\"submit\" name=\"confirmCustomer\">Confirm</button></td>
                                    </form>
                                </tr>";
                }
                return $output;
            }
            
            else {
                $output .= "<tr><td colspan='5'>No pending customers</td></tr>";
                return $output;
            }
        }

        function confirmCustomer() {        
            if(isset($_POST['confirmCustomer'])) {
                $db = connection();

                $username = $db->real_escape_string($_POST['username']);
                $password = $db->real_escape_string($_POST['password']);
                $id = $db->real_escape_string($_POST['id']);
                $type = 0;

                $sql = "UPDATE tbl_customer SET CONFIRMED='Yes' WHERE ID = ".$id."";

                $result = $db->query($sql);

                $stmt = $db->prepare("INSERT INTO `tbl_user` (CUSTOMER_ID, USERNAME, PASSWORD, U_TYPE) 
                VALUES ('$id', '$username', '$password', $type)");
                $stmt->bind_param("issi", $username, $password, $type);
                $stmt->execute();

                print $stmt->error; //to check errors
                $result = $stmt->affected_rows;
                $stmt->close();

                $db->close();

                if ($result == 1) {
                    redirect("confirmcustomer.php");
                }
                else {
                    print_r($sql);
                    return "<br><br>An Error has occured";
                    exit();
                }
            }  
    }

?>