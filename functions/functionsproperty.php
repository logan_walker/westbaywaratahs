<?php

    include_once('creds.php');

    /************************ display properties ***********************/

    function get_all_properties() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_properties";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "name" => $row['NAME'],
                "image" => $row['IMAGE_LINK'],
                "mowdate" => $row['MOWN_DATE'],
                "spraydate" => $row['SPRAYED_DATE'],
                "comments" => $row['COMMENTS']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    function show_properties($data) {
        
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                $output .= "<div class=\"row\" id=\"property-box\">
                <div class=\"col-md-3\">
                    <div class=\"products-padding\">
                        <h3>".$array[$i]['name']."</h3>
                        <img src=\"".$array[$i]['image']."\">
                    </div>   
                </div>
                <div class=\"col-md-2\">
                    <ul class=\"property-date\"><br>
                        <li><h3>Last Mown</h3></li><br><br>
                        <form method=\"POST\">
                            <input type=\"hidden\" name=\"id\" value=\"".$array[$i]['id']."\">
                            <li><input id=\"datepicker".$array[$i]['id']."\" type=\"text\" name=\"mowdate\" aria-describedby=\"basic-addon1\" value=\"".$array[$i]['mowdate']."\"/></li><br><br>
                            <li><button type=\"submit\" name=\"updateMownDate\">Update</button></li>
                        </form>
                    </ul>
                </div>
                <div class=\"col-md-3\">
                    <ul class=\"property-date\"><br>
                        <form method=\"POST\">
                            <input type=\"hidden\" name=\"id\" value=\"".$array[$i]['id']."\">  
                            <li><h3>Last Sprayed</h3></li><br><br>
                            <li><input id=\"datepickertwo".$array[$i]['id']."\" type=\"text\" name=\"spraydate\" aria-describedby=\"basic-addon1\" value=\"".$array[$i]['spraydate']."\"/></li><br><br>
                            <li><button type=\"submit\" name=\"updateSprayDate\">Update</button></li>
                        </form>
                    </ul>
                </div>
                <div class=\"col-md-4\">
                    <ul class=\"property-date\">
                        <form method=\"POST\">
                            <input type=\"hidden\" name=\"id\" value=\"".$array[$i]['id']."\">    
                            <li><h3>Comments</h3></li>
                            <li><textarea type=\"text\" name=\"comments\" style=\"margin-top:7px;\" cols=\"40\" rows=\"6\">".$array[$i]['comments']."</textarea></li>
                            <li style=\"padding-top:5px;\"><button type=\"submit\" name=\"updateComment\">Update</button></li>
                        </form>
                    </ul>
                </div>
            </div><br>";
            
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

    
    /********************* Update Properties **********************/
    function editMownDate() {
        
        if(isset($_POST['updateMownDate'])) {
            $db = connection();

            $mowdate = $db->real_escape_string($_POST['mowdate']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_properties SET MOWN_DATE='".$mowdate."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("property.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

    function editSprayDate() {
        
        if(isset($_POST['updateSprayDate'])) {
            $db = connection();

            $spraydate = $db->real_escape_string($_POST['spraydate']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_properties SET SPRAYED_DATE='".$spraydate."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("property.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

    function editComment() {
        
        if(isset($_POST['updateComment'])) {
            $db = connection();

            $comments = $db->real_escape_string($_POST['comments']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_properties SET COMMENTS='".$comments."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("property.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }
?>