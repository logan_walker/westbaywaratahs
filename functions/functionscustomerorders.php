<?php

    
    include_once('creds.php');

    function get_currentOrders() {            
        $db = connection();
        $userID = $_SESSION['id'];
        $sql = "SELECT * FROM tbl_order WHERE CUSTOMER_ID = '$userID' AND NOT STATUS = 'Paid'";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "cid" => $row['CUSTOMER_ID'],
                "delivery_date" => $row['DELIVERY_DATE'],
                "order_date" => $row['ORDER_DATE'],
                "item_ordered" => $row['ITEM_ORDERED'],
                "quantity_ordered" => $row['QUANTITY_ORDERED'],
                "cost_ordered" => $row['COST_ORDERED'],
                "status" => $row['STATUS']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }
    
    function get_currentOrdersAdmin() {            
        $db = connection();
        $userID = $_SESSION['id'];
        $sql = "SELECT * FROM tbl_order";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "cid" => $row['CUSTOMER_ID'],
                "delivery_date" => $row['DELIVERY_DATE'],
                "order_date" => $row['ORDER_DATE'],
                "status" => $row['STATUS']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    function get_orderHistory() {            
        $db = connection();
        $userID = $_SESSION['id'];
        $sql = "SELECT * FROM tbl_order WHERE CUSTOMER_ID = '$userID' AND STATUS = 'Paid'";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "cid" => $row['CUSTOMER_ID'],
                "delivery_date" => $row['DELIVERY_DATE'],
                "order_date" => $row['ORDER_DATE'],
                "status" => $row['STATUS']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    function get_currentOrderData() {            
        $db = connection();
        $sql = "SELECT * FROM tbl_product_ordered";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "order_id" => $row['ORDER_ID'],
                "item" => $row['ITEM'],
                "quantity" => $row['QUANTITY'],
                "cost" => $row['COST']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    function show_currentOrders($data) {
        $array = json_decode($data, True);
        $output = "";

        if (count($array) > 0 ) {
            $total = 1;
            for ($i = 0; $i < count($array); $i++) {
                $total = $total * $array[$i]['id'];
                $output .= "<tr>
                                <td><p>".$array[$i]['id']."</p></td>
                                <td><p>".$array[$i]['order_date']."</p></td>
                                <td><p>".$array[$i]['delivery_date']."</p></td>
                                <td><p>".$array[$i]['status']."</p></td>
                                <td><button id=\"view\" name=\"".$array[$i]['id']."\" data-toggle=\"modal\" data-target=\"#itemsModal\" onClick=\"viewItems(this.name)\">View</button></td>
                            </tr>";
                                                        
            }
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No orders to Display</td></tr>";
            return $output;
        }
    }

    function show_currentOrdersAdmin($data) {
        $array = json_decode($data, True);
        $output = "";

        if (count($array) > 0 ) {
            $total = 1;
            for ($i = 0; $i < count($array); $i++) {
                $total = $total * $array[$i]['id'];
                $output .= "<tr>
                                <td><p>".$array[$i]['id']."</p></td>
                                <td><p>".$array[$i]['cid']."</p></td>
                                <td><p>".$array[$i]['order_date']."</p></td>
                                <td><p>".$array[$i]['delivery_date']."</p></td>
                                <td><p>".$array[$i]['status']."</p></td>
                                <td><button id=\"view\" name=\"".$array[$i]['id']."\" onClick=\"viewItems(this.name)\">View</button></td>
                                <td><button id=\"viewAdmin\" name=\"".$array[$i]['id']."\" data-toggle=\"modal\" data-target=\"#itemsModal\" onClick=\"viewItemsAdmin(this.name)\">Change status</button></td>
                            </tr>";
                                                        
            }
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No orders to Display</td></tr>";
            return $output;
        }
    }
    

    function show_itemsOrdered($data) {
        $array = json_decode($data, True);
        $output = "";

        if (count($array) > 0 ) {
            $viewDisplay .= "<div id=\"items\"></div>";
            return $viewDisplay;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No current orders</td></tr>";
            return $output;
        }
    }

    function show_itemsOrderedAdmin($data) {
        $array = json_decode($data, True);
        $output = "";

        if (count($array) > 0 ) {
            $viewDisplay .= "<div id=\"itemsAdmin\"></div>";
            return $viewDisplay;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No current orders</td></tr>";
            return $output;
        }
    }


    function invoiceSend() {
        if(isset($_POST['invoiceSent'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            //Changed invoice sent to invoice created
            $sql = "UPDATE tbl_order SET STATUS='Invoice Created' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("currentorders.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }
    }

    function shipOrder() {
        if(isset($_POST['orderShipped'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_order SET STATUS='Shipped' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("currentorders.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }
    }

    function setOrderPaid() {
        if(isset($_POST['orderPaid'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_order SET STATUS='Paid' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("currentorders.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }
    }

?>