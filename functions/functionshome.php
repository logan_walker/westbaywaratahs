<?php

    include_once('creds.php');

        function get_productsHome() {
            
            $db = connection();
            $sql = "SELECT * FROM tbl_products WHERE NAME != 'A Product' ORDER BY name";
            $arr = [];

            $result = $db->query($sql);
        
            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }

            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "id" => $row['ID'],
                    "name" => $row['NAME'],
                    "image" => $row['IMAGE'],
                    "avalibility" => $row['AVALIBILITY']
                );
            }

            $json = json_encode($arr);
        
            $result->free();
            $db->close();
            
            return $json;
        }

        function show_productsHome($data) {
            $array = json_decode($data, True);
            $output = "";
            $aval = "";
            $labelclass = "";

            if (count($array) > 0 ) {
                for ($i = 0; $i < count($array); $i++) {
            
                    if ($array[$i]['avalibility'] == "no") {
                        $aval = "Out of Season";
                        $labelclass = "label label-warning";
                    } 
                    else if ($array[$i]['avalibility'] == "yes") {
                        $aval = "In Season";
                        $labelclass = "label label-success";
                    }
                    $output .= "<div class=\"col-md-4\">
                                    <div class=\"products-padding\">
                                        <img src=".$array[$i]['image'].">
                                        <h3>".$array[$i]['name']."</h3>
                                            <span class=\"$labelclass\">$aval</span>
                                    </div>
                                </div>";
                }
                return $output;
            }
            
            else {
                $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
                return $output;

        }

        function load_productsHome($id) {
            $db = connection();
            $sql = "SELECT * FROM tbl_products WHERE ID = $id";
            $arr = [];
            
            $result = $db->query($sql);
            
            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }
            
            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "id" => $row['ID'],
                    "name" => $row['NAME'],
                    "image" => $row['IMAGE']
                );
            }
            
            $json = json_encode($arr);
            $result->free();
            $db->close();
            return $json;        
        }
    }