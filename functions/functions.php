<?php

    include_once('creds.php');

    /** Connects website to Database **/

    function connection() {
        $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

        if ($conn->connect_errno > 0) {
            die('Unable to connect to database ['.$conn->connect_errno.']');
        } return $conn;
    }

    /* Login User */
    function loginUser() {

        if(isset($_POST['login'])) {
            $db = connection();
            
            global $user;
            $user = $db->real_escape_string($_POST['Username']);
            $pass = $db->real_escape_string($_POST['Password']);

            $sql = "SELECT * FROM tbl_user WHERE USERNAME = '$user' && PASSWORD = '$pass'";
            $arr = [];

            $result = $db->query($sql);

            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }
            
            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "user" => $row['USERNAME'],
                    "pass" => $row['PASSWORD'],
                    "type" => $row['U_TYPE'],
                    "cid" => $row['CUSTOMER_ID'],
                    "aid" => $row['ADMIN_ID']
                );
            }
            global $id;
            $aid = $arr[0]['aid'];
            $cid = $arr[0]['cid'];

            if ($cid == null){
                $id = $aid;
            }
            else {
                $id = $cid;
            }

            $type = $arr[0]['type'];
            $result->free();

            if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass'])) {
                $_SESSION['login'] = TRUE;
                $_SESSION['user'] = $user;
                $_SESSION['type'] = $type;
                $_SESSION['id'] = $id;
            }
            else {
                echo '<script language="javascript">';
                echo 'alert("Login attempt unsuccessful.")';
                echo '</script>';
                //echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
                redirect("index.php");
            }
            $db->close();
        }
    }

    function authenticateUser() {

    }

    function redirect($location) {
        $URL = $location;
        echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
        echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
        exit();
    }
    /** Register Customer **/

    function registerCustomer() {
        if(isset($_POST['register'])) {
            $db = connection();

            $username = $db->real_escape_string($_POST['Username']);
            $company = $db->real_escape_string($_POST['Company']);
            $fname = $db->real_escape_string($_POST['Fname']);
            $lname = $db->real_escape_string($_POST['Lname']);
            $email = $db->real_escape_string($_POST['Email']);
            $country = $db->real_escape_string($_POST['Country']);
            $address = $db->real_escape_string($_POST['Address']);
            $password = $db->real_escape_string($_POST['Password']);
            $type = 0;
            $confirmed = 'No';

            $stmt = $db->prepare("INSERT INTO `tbl_customer` (COMPANY_NAME, DELIVERY_ADDRESS, USERNAME, PASSWORD, F_NAME, L_NAME, COUNTRY, EMAIL, U_TYPE, CONFIRMED) 
                                    VALUES ('$company', '$address', '$username', '$password', '$fname', '$lname', '$country', '$email', $type, '$confirmed')");
            $stmt->bind_param("ssssssssis", $company, $address, $username, $password, $fname, $lname, $country, $email, $type, $confirmed);
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;
            $stmt->close();

            if ($result > 0) {
                echo '<script language="javascript">';
                echo 'alert("Account creation successful. You will be unable to sign in until your account has been confirmed. Your account will be confirmed within 3 working days.")';
                echo '</script>';
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }
    

    