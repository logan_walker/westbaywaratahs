<?php

    include_once('creds.php');

    function get_all_nav() {
        
        if($_SESSION['type'] == "1") {
            $db = connection();
            $sql = "SELECT * FROM tbl_nav WHERE USER_TYPE = 1";
            $arr = [];

            $result = $db->query($sql);

            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }

            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "link" => $row['LINK'],
                    "title" => $row['TITLE'],
                    "name" => $row['NAME'],
                );
            }

            $json = json_encode($arr);

            $result->free();
            $db->close();
            return $json;            
        }

        else {
            $db = connection();
            $sql = "SELECT * FROM tbl_nav WHERE USER_TYPE = 0";
            $arr = [];

            $result = $db->query($sql);

            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }

            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "link" => $row['LINK'],
                    "title" => $row['TITLE'],
                    "name" => $row['NAME'],
                );
            }

            $json = json_encode($arr);

            $result->free();
            $db->close();
            return $json;            
        }
    }
    
    function pushID() {        
        if(isset($_POST['pushID'])) {
            $db = connection();
            
            $id = $db->real_escape_string($_POST['id']);
            return $id;
        }  
    }

    function show_contentNav($data) {
        
        $array = json_decode($data, True);
        global $id;

        if(isset($_POST['pushID'])) {            
            $id = $db->real_escape_string($_POST['id']);
        }  
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                $output .= "<li><button onclick=\"location.href='".$array[$i]['link']."?id=".$_SESSION['id']."'\" type=\"button\" name=\"pushID\" id=\"".$array[$i]['name']."\" class=\"btn btn-lg btn-login navbuttons\">".$array[$i]['title']."</button></li>";
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

    
    