<?php

    include_once('creds.php');

        function get_productsOrder() {
            
            $db = connection();
            $sql = "SELECT * FROM tbl_products WHERE NAME != 'A Product' ORDER BY name";
            $arr = [];

            $result = $db->query($sql);
        
            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }

            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "id" => $row['ID'],
                    "name" => $row['NAME'],
                    "image" => $row['IMAGE'],
                    "cost" => $row['COST'],
                    "avalibility" => $row['AVALIBILITY']
                );
            }

            $json = json_encode($arr);
        
            $result->free();
            $db->close();
            
            return $json;
        }

        function show_productsOrder($data) {
            $array = json_decode($data, True);
            $table = "";
            $output = "";
            $orderTotal = "";
            $aval = "";
            $disable = "";
            $labelclass = "";
            $subtotal = 0;

            if (count($array) > 0 ) {
                for ($i = 0; $i < count($array); $i++) {
                    $itemTotal = 0;
                    if ($array[$i]['avalibility'] == "no") {
                        $aval = "Out of Season";
                        $labelclass = "label label-warning";
                        $disable = "disabled";
                    } 
                    else if ($array[$i]['avalibility'] == "yes") {
                        $aval = "In Season";
                        $labelclass = "label label-success";
                        $disable = "";
                    }
                    $output .= "<div class=\"col-md-4\">
                                    <div class=\"orderproducts-padding\">
                                        <img src=".$array[$i]['image'].">
                                        <h3>".$array[$i]['name']."</h3>
                                        <h5>$".$array[$i]['cost']." Per Box (excl. GST)</h5> 
                                        <span class=\"$labelclass\" id=\"centerAval\">$aval</span><br><br>
                                        <p>Quantity: <input id=\"quantity".$array[$i]['id']."\" type=\"number\" name=\"quantity\" min=\"0\" value=\"0\" style=\"width: 50px;\"  ".$disable.">
                                        <button id=\"addToOrder\" name=\"".$array[$i]['id']."\" onClick=\"addItem(this.name)\" ".$disable.">Add To Order</button></p>
                                    </div>
                                </div>";
                }
                return $output;
            }
            
            else {
                $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
                return $output;

            }
        }
