<?php   include_once('../functions/functions.php');
        include_once('../functions/functionshome.php');
        include_once('../functions/functionsnav.php');
        include_once('../functions/functionsmakeorder.php');
        session_start();
        loginUser(); 
        registerCustomer();
        //require('../fpdf.php');
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Westbay Waratahs</title>
        <link rel="icon" href="../images/imgtab.png">

        <!-- Bootstrap stylesheets -->
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="../css/styles.css" type="text/css">
        <link rel="stylesheet" href="../css/styles2.css" type="text/css">
        
        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.2/jspdf.plugin.autotable.js"></script>
        <script type="text/javascript" src="validation.min.js"></script>
        <script type="text/javascript" src="../javascript/script.js"></script>
    </head>
    <body>
    
    <!-- _________________________________navigation bar code starts here________________________ -->
    <!-- LOGIN/LOGIN CHECK -->

    <?php
    if(isset($_SESSION['login']))
    {
    ?>
    
    <!-- Nav shown to logged in user-->
    <nav class="navbar navbar-inverse newnav" style="text-align:center;">
        <div class="container-fluid">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown">
                    <a class="dropdown-toggle btn btn-lg btn-user" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>  <?php echo $_SESSION['user'];?>  <span class="caret"></span></a>
                    <ul class="dropdown-menu ">
                        <!-- <li><a href="../customer/account.php">Account settings</a></li> -->
                        <li><a href="../functions/logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <form role="form" method="post" class="pushid" id="pushid">
                <input type="hidden" name="id" value="<?php echo $_SESSION['id'];?>">
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <?php echo show_contentNav(get_all_nav());?>
                    </ul>
                </div>
            </form>
        </div>
    </nav>

    <?php
    }
    else
    {
    ?>

    <!-- Nav shown to not logged in user -->
    <nav class="navbar navbar-inverse" id="grad-nav">
        <div class="container-fluid">
            <ul class="nav navbar-nav navbar-right">
                <li><button type="button" name="login" id="login" class="btn btn-success" data-toggle="modal" data-target="#loginModal">Login</button>
                <!--<a href="#" role="button" data-toggle="modal" data-target="#login-modal"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>-->
            </ul>
        </div>
    </nav>

    <?php
    }
    ?>
    
    <!-- login modal here -->
    <div class="modal fade" id="loginModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- header -->
                <div class="modal-header" align="center">
                    <img class="img-circle" id="img_logo" src="../images/logo.png">
                    <button type"button" class="close" data-dismiss="modal">&times;</button>
                    <h2> Login </h2>
                </div>

                <div id="div-forms">

                    <?php 
                    if(!@$_SESSION['login']) 
                    {
                    ?>
                    
                    <form role="form" method="post" name="Login_Form" class="form-signin" id="login-form">
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="text" id="login_username" name="Username" class="form-control" placeholder="username" required="" autofocus=""/>
                                <input type="password" id="login_password" name="Password" class="form-control" placeholder="password" required=""/>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-primary btn-block" name="login" value="Login" type="Submit">Log in </button>
                        </div>
                    </form>

                    <?php
                    }
                    else
                    {
                    ?>
                        you are logged in
                    <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>

    <!-- Header -->
    <br>
    <br>
    <br>
    <header id="header">
        <div class="logo"><a href="../index/index.php">Westbay Waratahs <!--<br><br><span>New Zealand's largest foliage exporters</span>--></a></div>
    </header>

    <!-- Main -->
        <section id="main">
            <div class="inner">

        <!-- One -->
        <section id="one" class="wrapper style1">

            <!-- <div class="image fit flush">
                <img src="images/pic02.jpg" alt="" />
            </div> -->
            <header class="special">
                <h2>Make an Order</h2>
                <p>Welcome to the order page. On this page you can make an order, selecting the type and quantity of foliage you would like to purchase.</p>
            </header>
        </section>

        <!-- Main content starts here -->
        <div class="container">
            <div id="Products" class="productsOrderPage">
                <div class="row">
                    <div class="col-md-12">
                        <p class="mb-0">Please input the quantities for each of the items you wish to order.</p>
                    </div>
                </div>
                <div class="row">
                    <?php echo show_productsOrder(get_productsOrder());?>
                    <table id="orderitems" class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Select</th>
                                <th scope="col">Item</th>
                                <th scope="col">Box Quantity</th>
                                <th scope="col">Total Cost</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                                <!-- <li><button class="btn btn-primary btn-block">Place Order</button></li>
                                <li><br></li>
                                <li><button class="btn btn-danger btn-block">Cancel Order</button></li> -->
                    
                <div class="row">
                    <div class="col-md-4">
                        <table class="table">
                            <thead class="thead-inverse"> 
                                <tr>
                                <?php
                                if(isset($_SESSION['login']))
                                {
                                ?>
                                    <td scope="col"><button type="button" id="removeFromOrder" class="btn btn-danger" onClick="removeItem('orderitems')">Remove from Order</button></th>
                                    <td scope="col"><button type="button" name="<?php echo $_SESSION['id'];?>" id="makeOrder" class="btn btn-success" data-toggle="modal" data-target="#orderedItemsModal" onClick="makeOrder(this.name);">Confirm Order</button></th>
                                    <td scope="col">Delivery Date<input id="deliveryDate" type="text" aria-describedby="basic-addon1"/></td>
                                <?php 
                                }
                                ?>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-offset-4 col-md-4">
                        <table id="orderTotal" class="table">
                            <thead class="thead-inverse"> 
                                <tr>
                                    <td scope="col">Subtotal: <div id="subTotal">$0</div></th>
                                    <td scope="col">GST: <div id="gst">$0</div></th>
                                    <td scope="col">Grand Total: <div id="grandTotal">$0</div></th>
                                    <!-- <td scope="col">Confirm</th> -->
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <br><br><br><br>
            </div>
        </div>

        <div class="modal fade" id="orderedItemsModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- header -->
                    <div class="modal-header" align="center">
                        <button type"button" class="close" data-dismiss="modal">&times;</button>
                        <h2> Thanks for your order </h2>
                        <br>
                        <h3>Your order will be delivered on the</h3><h3 id="date"></h3>
                        <form action="../index/index.php"><input class="btn btn-primary btn-block" type="submit" value="Okay" /></form>
                    </div>
                </div>
            </div>
        </div>


            <!-- <form method="post">
                <input type="test" name="test_data">
                <input type="submit" name="clicked">
            </form>

                <?php
                    // if(isset($_POST['clicked']))
                    // {
                    //     class PDF extends FPDF
                    //     {
                    //     // Page header
                    //     function Header()
                    //     {
                    //         // Logo
                    //         $this->Image('../images/logo.png',10,6,30);
                    //         // Arial bold 15
                    //         $this->SetFont('Arial','B',15);
                    //         // Move to the right
                    //         $this->Cell(80);
                    //         // Title
                    //         $this->Cell(30,10,'Order invoice',4,0,'C');
                    //         // Line break
                    //         $this->Ln(20);
                    //     }
                        
                    //     // Page footer
                    //     function Footer()
                    //     {
                    //         // Position at 1.5 cm from bottom
                    //         $this->SetY(-15);
                    //         // Arial italic 8
                    //         $this->SetFont('Arial','I',8);
                    //         // Page number
                    //         $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
                    //     }
                    //     }
                    

                    // $pdf = new PDF();
                    // $pdf->AliasNbPages();
                    // $pdf->AddPage();
                    // $pdf->SetFont('Times','',12);
                    // $pdf->Cell(40,10,$_POST["test_data"]);
                    // $pdf->Output($dest="F",$name="../orderpdfs/".$_POST['test_data'].".pdf");
                    // }
                ?> -->

                
                
        <script type="text/javascript" src="../javascript/functions.js"></script>
    </body>
</html>